const gulp = require('gulp')
const purify = require('gulp-purifycss')

gulp.task('css', () => {
  return gulp.src('./dist/_nuxt/common.*.css')
    .pipe(purify(['./dist/**/*.js', './dist/**/*.html']))
    .pipe(gulp.dest('./dist/_nuxt'))
})
