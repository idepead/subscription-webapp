const trackerEmitters = {
  fbqEmit: function (...obj) {
    /* global fbq */
    /* eslint no-undef: ["error", { "typeof": true }] */
    try {
      fbq(...obj)
    } catch (e) {
      console.log(...obj)
    }
  }
}
module.exports = trackerEmitters
