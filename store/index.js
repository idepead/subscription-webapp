import Vue from 'vue'
import Vuex from 'vuex'

import courses from '~/assets/data/courses.json'

import { fbqEmit } from '~/utils/trackerEmitters'

Vue.use(Vuex)

const store = () => new Vuex.Store({
  state: {
    domain: {
      base: 'https://euquero.idepead.com.br',
      params: '/'
    },
    seo: {
      title: 'Eu quero!',
      description: 'Ingresse agora mesmo em um dos cursos oferecidos pelo IDEPead.',
      image: '/assets/images/unifran_og.jpg'
    },
    courses,
    coursesArray: courses.map((c) => c.slug),
    searchWord: '',
    avalibleCourses: courses,
    filteredCourses: null,

    filterTags: {
      type: ['bacharel', 'tecnólogo', 'licenciatura'],
      time: [2, 4, 6, 8, 10]
    },

    subscription: {
      name: null,
      email: null,
      phone: null,
      cpf: null,
      birthday: null,
      agreed: true,
      course: null,
      send: {
        success: false,
        error: false
      }
    },

    actions: {
      loading: false
    }
  },
  getters: {

    getSeoDomain: (state) => (state.domain.base + state.domain.params),
    getSeoTitle: (state) => state.seo.title,
    getSeoDescription: (state) => state.seo.description,
    getSeoImage: (state) => state.seo.image,

    allCourses: (state) => state.avalibleCourses,

    getCoursesArray: (state) => state.coursesArray,
    getSearchWord: (state) => state.searchWord,
    getFilteredCourses: (state) => state.filteredCourses,
    getAvalibleCourses: (state) => state.avalibleCourses,

    getFilteredTagByType: (state) => state.filterTags.type,
    getFilteredTagByTime: (state) => state.filterTags.time,
    getFilteredTagByMethod: (state) => state.filterTags.method,

    getActionLoading: (state) => state.actions.loading,

    getSubscription: (state) => state.subscription,

    getSubscriptionName: (state) => state.subscription.name,
    getSubscriptionEmail: (state) => state.subscription.email,
    getSubscriptionPhone: (state) => state.subscription.phone,
    getSubscriptionCPF: (state) => state.subscription.cpf,
    getSubscriptionBirthday: (state) => state.subscription.birthday,
    getSubscriptionAgreed: (state) => state.subscription.agreed,
    getSubscriptionCourse: (state) => state.subscription.course,
    getSubscriptionCourseString: (state) => JSON.stringify(state.subscription.course),

    getSubscriptionSendSuccess: (state) => state.subscription.send.success,
    getSubscriptionSendError: (state) => state.subscription.send.error

  },
  mutations: {
    SET_SEO_DOMAIN (state, params) {
      state.domain.params = params
    },
    SET_SEO_TITLE (state, title) {
      state.seo.title = title
    },
    SET_SEO_DESCRIPTION (state, description) {
      state.seo.description = description
    },
    SET_SEO_IMAGE (state, url) {
      state.seo.description = url
    },

    FILTER_COURSES (state, word) {
      word = (!word ? state.searchWord : word)
      state.searchWord = word
      word = word.trim().toLowerCase()
      state.filteredCourses = state.avalibleCourses.filter((course) => {
        return course.name.toLowerCase().includes(word)
      })
    },
    SET_FILTERED_COURSE (state, payload) {
      const fbqObj = {
        search_string: payload.slug
      }
      fbqEmit('track', 'Search', fbqObj)
      state.subscription.course = payload
    },

    MODIFY_AVALIBLE_COURSES (state, tags) {
      state.filterTags[tags.name] = (tags.name === 'time' ? tags.value.map(number => parseInt(number)) : tags.value)

      let type = state.filterTags.type
      let time = state.filterTags.time

      state.avalibleCourses = state.courses.filter((course) => {
        return (type.includes(course.type) && time.includes(course.time))
      })
    },

    SET_ACTION_LOADING (state, bool) {
      state.actions.loading = bool
    },

    SET_SUBSCRIPTION_NAME (state, name) {
      state.subscription.name = name
    },
    SET_SUBSCRIPTION_EMAIL (state, email) {
      state.subscription.email = email
    },
    SET_SUBSCRIPTION_PHONE (state, phone) {
      state.subscription.phone = phone
    },
    SET_SUBSCRIPTION_CPF (state, cpf) {
      state.subscription.cpf = cpf
    },
    SET_SUBSCRIPTION_BIRTHDAY (state, birthday) {
      state.subscription.birthday = birthday
    },
    SET_SUBSCRIPTION_AGREED (state, agreed) {
      state.subscription.agreed = agreed
    },
    SET_SUBSCRIPTION_COURSE (state, payload) {
      const fbqObj = {
        value: payload.priceOff,
        currency: 'BRL',
        content_name: payload.name,
        content_type: payload.type,
        content_ids: [state.coursesArray.indexOf(payload.slug)]
      }
      fbqEmit('track', 'AddToCart', fbqObj)
      state.subscription.course = payload
    },
    SET_SUBSCRIPTION_SEND (state, payload) {
      const fbqObj = {
        value: state.subscription.priceOff,
        currency: 'BRL',
        content_name: state.subscription.slug,
        content_category: 'courses'
      }
      fbqEmit('track', 'Lead', fbqObj)
      state.subscription.send[payload.name] = Boolean(payload.value)
    },

    CLEAN_SUBSCRIPTION (state) {
      state.subscription = {
        name: null,
        email: null,
        phone: null,
        cpf: null,
        birthday: null,
        agreed: true,
        course: null,
        send: {
          success: false,
          error: false
        }
      }
    }

  },
  actions: {
    SET_SEO_DOMAIN ({ commit }, domain) {
      commit('SET_DOMAIN', domain)
    },
    SET_SEO_TITLE ({ commit }, title) {
      commit('SET_TITLE', title)
    },
    SET_SEO_DESCRIPTION ({ commit }, description) {
      commit('SET_SEO_DESCRIPTION', description)
    },
    SET_SEO_IMAGE ({ commit }, url) {
      commit('SET_SEO_IMAGE', url)
    },

    FILTER_COURSES ({ commit }, word) {
      commit('FILTER_COURSES', word)
    },
    SET_FILTERED_COURSE ({ commit }, course) {
      commit('SET_FILTERED_COURSE', course)
    },

    MODIFY_AVALIBLE_COURSES ({ commit }, tags) {
      commit('MODIFY_AVALIBLE_COURSES', tags)
      commit('FILTER_COURSES')
    },

    SET_SUBSCRIPTION_NAME ({ commit }, name) {
      commit('SET_SUBSCRIPTION_NAME', name)
    },
    SET_SUBSCRIPTION_EMAIL ({ commit }, email) {
      commit('SET_SUBSCRIPTION_EMAIL', email)
    },
    SET_SUBSCRIPTION_PHONE ({ commit }, phone) {
      commit('SET_SUBSCRIPTION_PHONE', phone)
    },
    SET_SUBSCRIPTION_CPF ({ commit }, cpf) {
      commit('SET_SUBSCRIPTION_CPF', cpf)
    },
    SET_SUBSCRIPTION_BIRTHDAY ({ commit }, birthday) {
      commit('SET_SUBSCRIPTION_BIRTHDAY', birthday)
    },
    SET_SUBSCRIPTION_AGREED ({ commit }, agreed) {
      commit('SET_SUBSCRIPTION_AGREED', agreed)
    },
    SET_SUBSCRIPTION_COURSE ({ commit }, course) {
      commit('SET_SUBSCRIPTION_COURSE', course)
    },
    SET_SUBSCRIPTION_SEND ({ commit }, course) {
      commit('SET_SUBSCRIPTION_SEND', course)
    },

    CLEAN_SUBSCRIPTION ({ commit }) {
      commit('CLEAN_SUBSCRIPTION')
    }
  }
})

export default store
