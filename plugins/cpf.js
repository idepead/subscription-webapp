import { Validator } from 'vee-validate'

import cpf from 'gerador-validador-cpf'

Validator.extend('cpf', {
  getMessage: field => 'The ' + field + ' value is not valid.',
  validate: value => cpf.validate(value)
})
