export default ({ app }) => {
  /*
  ** Only run on client-side and only in production mode
  */
  if (process.env.NODE_ENV !== 'production') return
  /*
  ** Every time the route changes (fired on initialization too)
  */
  app.router.afterEach((to, from) => {
    /*
    ** We tell Google Analytics to add a `pageview`
    */
    /* global ga */
    /* eslint no-undef: ["error", { "typeof": true }] */
    ga('set', 'page', to.fullPath)
    ga('send', 'pageview')
  })
}
