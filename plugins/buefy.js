import Vue from 'vue'
import Buefy from 'buefy'
Vue.use(Buefy, {
  defaultIconPack: 'fa',
  defaultDayNames: ['D', 'S', 'T', 'Qa', 'Qi', 'Se', 'Sa'],
  defaultMonthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro']
})
